import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');


document.querySelector("a.logo").innerHTML+=`<small>Les pizza c'est la vie</small>`;
const attri=document.querySelector("header").querySelector("a.pizzaListLink").getAttribute("class")+" active";
document.querySelector("header").querySelector("a.pizzaListLink").setAttribute("class",attri);
document.querySelector(".newsContainer").setAttribute("style","");

function closeNew( event ) {
	document.querySelector(".newsContainer").setAttribute("style","display:none");
}
const buttonClose = document.querySelector('.closeButton');
buttonClose.addEventListener('click', closeNew);

Router.menuElement = document.querySelector('.mainMenu');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component('section', null, 'Ici vous pourrez ajouter une pizza');

Router.routes = [
    { path: '/', page: pizzaList, title: 'La carte' },
    { path: '/a-propos', page: aboutPage, title: 'À propos' },
    { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas





